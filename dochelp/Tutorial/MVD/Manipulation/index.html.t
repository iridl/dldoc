<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
 xml:lang="en"
      >
<head>
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Manipulating Data</title>
   <meta name="author" content="Emily K. Grover" />
<link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
<link rel="stylesheet" type="text/css" href="/localconfig/ui.css" />
<link rel="stylesheet" type="text/css" href="/dochelp/doc.css" />
<link rel="canonical" href="index.html" />
      <meta property="maproom:Sort_Id" content="mvd01" />
<link class="carryLanguage" rel="home" href="http://iri.columbia.edu/" title="IRI" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="/localconfig/navmenu.json" />
<link rel="shortcut icon" href="/uicore/icons/iri32.png" />
<link rel="apple-touch-icon" sizes="54x54" href="/uicore/icons/iriwh.png" />
<link rel="icon" href="/uicore/icons/iriwh.svg" sizes="any" type="image/svg+xml" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#tutorial" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#tutorialMVD" />
<script type="text/javascript" src="/uicore/uicore.js"></script>
<script type="text/javascript" src="/localconfig/ui.js"></script>
</head>
<body>
<form name="pageform" id="pageform">
<input class="carryLanguage" name="Set-Language" type="hidden" />
<input class="titleLink itemImage" name="bbox" type="hidden" />
<input class="map" name="bbox" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList">
                <legend>Help Resources</legend>
                      <a rev="section" class="navlink carryup" href="/dochelp/Tutorial/">Tutorial</a>
            </fieldset>
            <fieldset class="navitem" id="chooseSection" about="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#tutorialMVD">
                <legend property="term:label">Manipulate, Visualize, Download</legend>
            </fieldset>
 </div>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
<li> <a HREF="#Intro"> Introduction</a></li>
<li> <a HREF="#Arithmetic"> Arithmetic</a></li>
<li> <a HREF="#Limits"> Limits</a></li>
<li> <a HREF="#Averages">Averages</a></li>
<li> <a HREF="#Other">Statistical and Other</a></li>
</ul>
<div id="Intro" class="ui-tabs-panel">
<h2 property="term:title">Manipulating Data</h2>
<p property="term:description">
This section covers arithmetic operations, setting limits, averages, as well as some other analyses.</p>
<span class="t">Note that when specific time periods or locations are not selected, these operations are applied to the time and spatial grids in their entirety by default. It also important to note that when multiple data variables are to be compared, as is common in the examples in this section, the time and spatial grids of those variables must be identical. You will see this issue addressed frequently.</span>
</div>
<div id="Arithmetic" class="ui-tabs-panel">
<h2>Basic Arithmetic Operations</h2>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
<li><a href="#Add_number">Adding number to field</a></li>
<li><a href="#Add_field">Adding fields</a></li>
<li><a href="#Sub_number">Subtracting number from field</a></li>
<li><a href="#Sub_field">Subtracting fields</a></li>
<li><a href="#Mul_number">Multiplying field by number</a></li>
<li><a href="#Mul_field">Multiplying fields</a></li>
<li><a href="#Div_number">Dividing field by number</a></li>
<li><a href="#Div_field">Dividing fields</a></li>
</ul>
<div id="Add_number" class="ui-tabs-panel">
<h2>Adding a number to a field</h2>
<span class="ex"><u>Example: Add 2.5°<<<MVDparameterText1>>> to the minimum temperature data from <<<MVDparameterText2>>>.</u></span>
<p>
<span class="instruct">Start at the <a href="<<<MVDparameterHref1>>>"><<<MVDparameterText3>>>*</a> dataset main page.</span>
</p>
<span class="instruct">Select <<<MVDparameterText4>>> <<<MVDparameterText2>>>. </span> <font size="-1"><a href="<<<MVDparameterHref1>>><<<MVDparameterHref2>>>">CHECK</a></font>
<br /><span class="instruct">Select the minimum temperature data variable. </span>
<font size="-1"><a href="<<<MVDparameterHref1>>><<<MVDparameterHref2>>><<<MVDparameterHref3>>>">CHECK</a>
<a href="/expert<<<MVDparameterHref1>>><<<MVDparameterHref2>>><<<MVDparameterHref3>>>#expert">EXPERT</a></font>
<br /><span class="t">When adding a number to the field, the units of that data variable are automatically used. Note the units of the minimum temperature data variable under the Other Info heading.</span>
<p>
<a href="<<<MVDparameterHref1>>><<<MVDparameterHref2>>><<<MVDparameterHref3>>>">START</a>
</p>
<p>
<span class="instruct">While in expert mode, enter the following line
below the text already there.</span>
<pre class="ingridcode">2.5 add</pre>
<span class="instruct">Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref1>>><<<MVDparameterHref2>>><<<MVDparameterHref3>>>2.5/add/#expert">CHECK</a></font>
</p><p>
<span class="t">To see the results of this operation:</span>
<br /><span class="instruct">Select Tables link > Agree button> columnar table link. </span>
<font size="-1"><a href="/expert<<<MVDparameterHref1>>><<<MVDparameterHref2>>><<<MVDparameterHref3>>>2.5/add/T+exch+table-+text+text+skipanyNaN+-table+.html">CHECK</a></font>
<span class="instruct">Compare with </span><font size="-1"><a href="<<<MVDparameterHref1>>><<<MVDparameterHref2>>><<<MVDparameterHref3>>>T+exch+table-+text+text+skipanyNaN+-table+.html">ORIGINAL DATA</a></font>
</p>
</div>
<div id="Add_field" class="ui-tabs-panel">
<h2>Adding fields</h2>
<span class="ex"><u>Example: Recreate the observed monthly <<<MVDparameterText5>>> data by adding the climatological <<<MVDparameterText5>>> data and the <<<MVDparameterText5>>> anomaly data.</u></span>
<p>
<span class="instruct">Start at the <a href="<<<MVDparameterHref4>>>"><<<MVDparameterText6>>>*</a>
dataset main page.</span>
</p><p>
<span class="t">Because we are combining an observational data stream with a climatological data stream, we do not need to worry about the time grids matching. We must only make sure that the two streams have the same time scale (temporal resolution). In this example, the time grids of each of the data variables look like this:</span>
</p><p>
<span class="t"><u>Climatological <<<MVDparameterText5>>></u>
<br />Time grid: /T <<<MVDparameterText7b>>></span>
</p><p>
<span class="t"><u>Anomalies</u>
<br />Time grid: /T <<<MVDparameterText7>>></span>
</p><p>
<span class="t">Note that both of the data variables have a the same time scale and that the time grid for the climatological data is periodic. Ingrid will automatically match that periodic data properly with the anomalies time grid.</span>
</p><p>
<span class="instruct">Select (and compute anomalies if necessary) the <<<MVDparameterText8>>> and climatological <<<MVDparameterText5>>> data variables. </span>
<font size="-1"> <a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref5>>><<<MVDparameterHref4>>><<<MVDparameterHref6>>>#expert">EXPERT</a></font>
</p><p>
<a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref5>>><<<MVDparameterHref4>>><<<MVDparameterHref6>>>#expert">START</a>
</p><p><span class="instruct">While in expert mode, enter the following line below the text already there.</span>
<pre class="ingridcode">add</pre>
<span class="instruct">Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref5>>><<<MVDparameterHref4>>><<<MVDparameterHref6>>>add/#expert">CHECK</a></font>
</p><p>
<span class="t">To see the results of this operation:</span>
<br /><span class="instruct">Select a grid point (<<<MVDparameterText9>>>) to make the size of the data file more manageable. </span>
<font size="-1"><a href="<<<MVDparameterHref4>>><<<MVDparameterHref5>>><<<MVDparameterHref4>>><<<MVDparameterHref6>>>add/Y/%28<<<MVDparameterText10>>>%29VALUE/X/%28<<<MVDparameterText11>>>%29VALUE/">CHECK</a>
<a href="<<<MVDparameterHref4>>><<<MVDparameterHref5>>><<<MVDparameterHref4>>><<<MVDparameterHref6>>>add/Y/%28<<<MVDparameterText10>>>%29VALUE/X/%28<<<MVDparameterText11>>>%29VALUE/">START</a></font>
<br /><span class="instruct">Select Tables link > Agree button> columnar table link.</span>
<font size="-1"> <a href="<<<MVDparameterHref4>>><<<MVDparameterHref5>>><<<MVDparameterHref4>>><<<MVDparameterHref6>>>add/Y/%28<<<MVDparameterText10>>>%29VALUE/X/%28<<<MVDparameterText11>>>%29VALUE/ngridtable+table-+skipanyNaN+1+-table+.html">CHECK</a></font>
<span class="instruct">Compare with</span><font size="-1"> <a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref5>>>/Y/%28<<<MVDparameterText10>>>%29VALUE/X/%28<<<MVDparameterText11>>>%29VALUE/ngridtable+table-+skipanyNaN+1+-table+.html">CLIMO DATA</a></font>
<span class="instruct">,</span>
<font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref6>>>Y/%28<<<MVDparameterText10>>>%29VALUE/X/%28<<<MVDparameterText11>>>%29VALUE/ngridtable+table-+skipanyNaN+1+-table+.html">ANOMALY DATA</a></font>
<span class="instruct">and</span>
<font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref5>>><<<MVDparameterHref4>>><<<MVDparameterHref6>>>add/Y/%28<<<MVDparameterText10>>>%29VALUE/X/%28<<<MVDparameterText11>>>%29VALUE/ngridtable+table-+skipanyNaN+1+-table+.html">OBSERVED DATA</a></font>
<span class="instruct">.</span>
</p>
</div>
<div id="Sub_number" class="ui-tabs-panel">
<h2>Subtracting a number from a field</h2>
<span class="ex"><u>Example: Subtract 2.5°<<<MVDparameterText1>>> from the minimum temperature data from <<<MVDparameterText2>>>.</u></span>
<br /><span class="t">Refer to the example in Section Adding a Number to a Field.</span>
<p>
<span class="instruct">Substitute the following Ingrid command for <i>2.5 add</i>.</span>
<pre class="ingridcode">2.5 sub</pre>
<span class="instruct">Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref1>>><<<MVDparameterHref2>>><<<MVDparameterHref3>>>2.5/sub/#expert">CHECK</a></font>
<br /><span class="t">Note that Ingrid subtracts the second number/field listed (e.g., 2.5) from the first number/field listed (e.g., min. temperature).</span>
</p>
</div>
<div id="Sub_field" class="ui-tabs-panel">
<h2>Subtracting fields</h2>
<p><span class="ex"><u>Example: Create the monthly <<<MVDparameterText5>>> anomalies data by subtracting the monthly climatological <<<MVDparameterText5>>> data from the observed monthly <<<MVDparameterText5>>> data.</u></span>
</p><p>
<span class="instruct">Start at the <a href="<<<MVDparameterHref4>>>"><<<MVDparameterText6>>>*</a> dataset main page.</span>
</p><p>
<span class="t">Note the time grids of the two variables to be compared.</span>
</p><p>
<span class="t"><u><<<MVDparameterText5>>></u>
<br />Time grid: /T <<<MVDparameterText7>>></span>
</p><p>
<span class="t"><u>Climatology</u>
<br />Time grid: /T (months since 01-Jan) periodic Jan to Dec by 1. N= 12 pts :grid</span>
</p><p>
<span class="t">As is common when comparing two variables from the same dataset, their time grids match exactly. However, you should get always make a of point of checking this.</span>
</p><p>
<span class="instruct">Make sure that the spatial grids of these two variables match.</span>
<br /><span class="t">Now that we are sure that the grids match properly, this example is very much like that in Section <a href="#Add_field">1.b</a> where we added the two fields. The primary difference here is the order that the variables are listed. As noted in Section <a href="#Sub_number">1.c</a>, Ingrid subtracts the second field listed from the first field listed.</span>
</p><p>
<span class="instruct">Select the monthly <<<MVDparameterText5>>> and its climatology data variables. </span>
<font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref7>>><<<MVDparameterHref4>>><<<MVDparameterHref5>>>/#expert">EXPERT</a></font>
</p><p>
<a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref7>>><<<MVDparameterHref4>>><<<MVDparameterHref5>>>/#expert">START</a>
</p><p>
<span class="instruct">While in expert mode, enter the following line below the
text already there.</span>
<pre class="ingridcode">sub</pre>
<span class="instruct">Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref7>>><<<MVDparameterHref4>>><<<MVDparameterHref5>>>/sub/#expert">CHECK</a></font>
</p>
</div>
<div id="Mul_number" class="ui-tabs-panel">
<h2>Multiplying a field by a number</h2>
<p>
<span class="ex"><u>Example: Convert the units of the <<<MVDparameterText12>>> data from <<<MVDparameterText13>>> to <<<MVDparameterText14>>> by multiplying the field by <<<MVDparameterText15>>>.</u></span>
<br /><span class="t">Note: there is an Ingrid command that converts units themselves instead of just the data values. This is just an example and the units of the data will still appear as <<<MVDparameterText13>>> after the arithmetic operation.</span>
</p><p>
<span class="instruct">Start at the <a href="<<<MVDparameterHref4>>>"><<<MVDparameterText6>>>*</a> dataset main page.
<br />Select the <<<MVDparameterText12>>> data variable. </span><font size="-1"><a href="<<<MVDparameterHref4>>><<<MVDparameterHref8>>>">CHECK</a>
<a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/#expert">EXPERT</a></font>
<br /><span class="instruct">While in expert mode, enter the following line below the text already there.</span>
<pre class="ingridcode"><<<MVDparameterText15>>> mul</pre><span class="instruct">Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref8>>><<<MVDparameterText15>>>/mul/#expert">CHECK</a></font>
</p>
</div>
<div id="Mul_field" class="ui-tabs-panel">
<h2>Multiplying fields</h2>
<p>
<span class="t">This operation works just like that covered in Sections <a href="#Add_field">1.b</a> and <a href="#Sub_field">1.d.</a></span>
</p><p>
<span class="t">Ensure that the grids of the variables match, select both of them, and use <span class="ingridcode">mul</span> as the operator in expert mode. The <span class="ingridcode">mul</span> command can also be used to find common entries in two different data streams.</span>
</p>
</div>
<div id="Div_number" class="ui-tabs-panel">
<h2>Dividing a field by a number</h2>
<p>
<span class="ex"><u>Example: Convert the units of the precipitation data from <<<MVDparameterText16>>> to <<<MVDparameterText17>>> by dividing the field by <<<MVDparameterText18>>>.</u></span>
<br /><span class="t">Note: there is an Ingrid command that converts units themselves instead of just the data values. This is just an example and the units of the data will still appear as <<<MVDparameterText16>>> after the arithmetic operation.</span>
</p><p>
<span class="instruct">Start at the <a href="<<<MVDparameterHref4b>>>"><<<MVDparameterText6>>>*</a> dataset main page.
<br />Select the precipitation data variable. </span><font size="-1"><a href="<<<MVDparameterHref4b>>><<<MVDparameterHref7b>>>">CHECK</a> <a href="/expert<<<MVDparameterHref4b>>><<<MVDparameterHref7b>>>#expert">EXPERT</a></font>
<br /><span class="instruct">While in expert mode, enter the following line below the text already there.</span>
<pre class="ingridcode"><<<MVDparameterText18>>> div</pre><span class="instruct">Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref4b>>><<<MVDparameterHref7b>>><<<MVDparameterText18>>>/div/#expert">CHECK</a></font>
</p><p>
<span class="t">Note that Ingrid divides the first number/field listed (e.g., precipitation) by the second number/field listed (e.g. <<<MVDparameterText18>>>).</span>
</p>
</div>
<div id="Div_field" class="ui-tabs-panel">
<h2>Dividing fields</h2>
<p>
<span class="t">This operation works just like that covered in Sections <a href="#Add_field">1.b</a> and <a href="#Sub_field">1.d.</a></span>
</p><p>
<span class="t">Ensure that the grids of the variables match, select both of them, and use <span class="ingridcode">div</span> as the operator in expert mode. Again, note that Ingrid divided the first field listed by the second field listed.</span>
</p>
</div>
</div>
</div>
<div id="Limits" class="ui-tabs-panel">
<h2>Setting Limits</h2>
<span class="t">It is often useful to limit data values. You may want to set minimum and maximum limits on data as a means of quality control or only use data that meets particular criteria. Ingrid makes these types of operations very easy. Below are some common examples.</span>
<br />
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
<li><a href="#Set">Setting a minimum/maximum</a></li>
<li><a href="#Find">Finding a minimum/maximum</a></li>
<li><a href="#Mask">Creating a numerical mask</a></li>
<li><a href="#Flag">Flagging Data</a></li>
</ul>
<div id="Set" class="ui-tabs-panel">
<h2>Setting a minimum/maximum</h2>
<p>
<span class="ex"><u>Example: Create a data stream where all minimum temperature data values less than <<<MVDparameterText19>>>°C are given a value of <<<MVDparameterText19>>>°C.</u></span>
</p><p>
<span class="instruct">Start at the <a href="<<<MVDparameterHref1>>>"><<<MVDparameterText3>>>*</a> dataset main page.
<br />Select the minimum temperature data variable. </span><font size="-1"> <a href="<<<MVDparameterHref1>>><<<MVDparameterHref3>>>">CHECK</a> <a href="/expert<<<MVDparameterHref1>>><<<MVDparameterHref3>>>#expert">EXPERT</a></font>
</p><p>
<span class="t">As previously described, it is a good idea to note the units of the data in question as all values in Ingrid are automatically referenced to the units of the data variable.</span>
</p><p>
<span class="instruct">Note that units of temperature by looking at the information under the Other Info heading.</span>
<br /><span class="t">In this case, the units are in <<<MVDparameterText20>>> and we must therefore give our desired minimum temperature in <<<MVDparameterText20>>>.</span>
</p><p>
<span class="instruct">While in expert mode, enter the following line below the text already there.</span>
<pre class="ingridcode"><<<MVDparameterText21>>> max</pre><span class="instruct">Click "OK".</span>
</p><p>
<span class="t">To see the results of this operation:</span>
<br /><span class="instruct">Select a single <<<MVDparameterText22>>> and short time period (e.g., 1996) to make the size of the data file more manageable. </span>
<font size="-1"> <a href="<<<MVDparameterHref1>>><<<MVDparameterHref3>>><<<MVDparameterText21>>>/max/<<<MVDparameterHref9>>>T/%281%20Jan%201996%29%2831%20Dec%201996%29RANGE/">CHECK</a></font>
</p><p>
<a href="<<<MVDparameterHref1>>><<<MVDparameterHref3>>><<<MVDparameterText21>>>/max/<<<MVDparameterHref9>>>T/%281%20Jan%201996%29%2831%20Dec%201996%29RANGE/">START</a>
</p><p>
<span class="instruct">Select Tables link > Agree button> columnar table link. </span>
<font size="-1"><a href="<<<MVDparameterHref1>>><<<MVDparameterHref3>>><<<MVDparameterText21>>>/max/<<<MVDparameterHref9>>>T/%281%20Jan%201996%29%2831%20Dec%201996%29RANGE/T+exch+table-+text+text+skipanyNaN+-table+.html">CHECK</a></font>
<span class="instruct">Compare with the</span> <font size="-1"><a href="/expert<<<MVDparameterHref1>>><<<MVDparameterHref3>>><<<MVDparameterHref9>>>T/%281%20Jan%201996%29%2831%20Dec%201996%29RANGE/T+exch+table-+text+text+skipanyNaN+-table+.html">ORIGINAL DATA</a></font>
<span class="instruct">.</span>
</p><p>
<span class="t">An analogous operation, setting a maximum value of <<<MVDparameterText19>>>°C, can be done by replacing the command <i><<<MVDparameterText21>>> max</i> with <span class="ingridcode"><<<MVDparameterText21>>> min</span>.</span>
</p>
</div>
<div id="Find" class="ui-tabs-panel">
<h2>Finding a minimum/maximum</h2>
<p>
<span class="t">There are two common uses of these feature. You may want to find a minimum/maximum value in a particular region or time period. Let us look at examples of these operations.</span>
</p><p>
<span class="ex"><u>Example: Find the largest <<<MVDparameterText8>>> anomalies for the entire time grid.</u></span>
<br /><span class="t">This example finds the largest <<<MVDparameterText8>>> anomalies from the entire time grid for each grid point. The result is the largest <<<MVDparameterText8>>> anomalies as function of X (longitude) and Y (latitude). Of course, you can limit the time grid to find the largest <<<MVDparameterText8>>> anomalies in a more specific time period.</span>
</p><p>
<span class="instruct">Start at the <a href="<<<MVDparameterHref4>>>"><<<MVDparameterText6>>>*</a> dataset main page.
<br />Select the monthly <<<MVDparameterText8>>> data variable (and compute anomalies if necessary). </span><font size="-1"><a href="<<<MVDparameterHref4>>><<<MVDparameterHref6>>>">CHECK</a> <a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref6>>>#expert">EXPERT</a></font>
</p><p>
<span class="t">To find the largest <<<MVDparameterText8>>> anomalies:</span>
<br /><span class="instruct">While in expert mode, enter the following line below the text already there.</span>
<pre class="ingridcode">[T] maxover</pre><span class="instruct">Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref6>>>%5BT%5Dmaxover/#expert">CHECK</a></font>
</p><p>
<span class="t">To find the largest negative <<<MVDparameterText8>>> anomalies:</span>
<br /><span class="instruct">While in expert mode, enter the following line below the text already there.</span>
<pre class="ingridcode">[T] minover</pre><span class="instruct">Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref6>>>%5BT%5Dminover/#expert">CHECK</a></font>
</p><p><span class="t">To see the results of this operation:</span>
<br /><span class="instruct">Select the options colors with coasts in the Views tab. </span><font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref6>>>%5BT%5Dminover/figviewer.html?map.url=X+Y+fig-+colors+coasts+-fig&amp;my.help=more+options">CHECK</a></font>
</p><p>
<span class="ex"><u>Example: Find the largest <<<MVDparameterText8>>> anomalies for the entire spatial grid.</u></span>
<br /><span class="t">This example finds the largest <<<MVDparameterText8>>> anomalies from the entire spatial grid for each time step. The result is the maximum global <<<MVDparameterText8>>> anomalies as a function of T (time). Of course, you can limit the spatial grid to find the largest <<<MVDparameterText8>>> anomalies in a specific region.</span>
</p><p>
<span class="instruct">Start at the <a href="<<<MVDparameterHref4>>>"><<<MVDparameterText6>>>*</a>
dataset main page.
<br />Select the monthly <<<MVDparameterText8>>> data variable (and compute anomalies if necessary). </span><font size="-1"><a href="<<<MVDparameterHref4>>><<<MVDparameterHref6>>>">CHECK</a> <a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref6>>>#expert">EXPERT</a></font>
<br /><span class="t">To find the largest  <<<MVDparameterText8>>> anomalies:</span>
<br /><span class="instruct">While in expert mode, enter the following line below the text already there.</span>
<pre class="ingridcode">[X Y] maxover</pre><span class="instruct">Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref6>>>%5BX/Y%5Dmaxover/#expert">CHECK</a></font>
</p><p>
<span class="t">To find the largest negative <<<MVDparameterText8>>> anomalies:</span>
<br /><span class="instruct">While in expert mode, enter the following line below the text already there.</span>
<pre class="ingridcode">[X Y] minover</pre><span class="instruct">Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref6>>>%5BX/Y%5Dminover/#expert">CHECK</a></font>
</p><p>
<span class="t">To see the results of this operation:</span>
<br /><span class="instruct">Select Tables tab > columnar table link.</span><font size="-1"> <a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref6>>>%5BX/Y%5Dminover/T+exch+table-+text+text+skipanyNaN+-table+.html">CHECK</a></font>
</p>
</div>
<div id="Mask" class="ui-tabs-panel">
<h2>Creating a numerical mask</h2>
<p>
<span class="t">Masks make data values that meet a particular threshold equal to NaN.</span>
</p><p>
<span class="ex"><u>Example: Mask out the maximum temperature values greater than <<<MVDparameterText23>>>°<<<MVDparameterText1>>>.</u></span>
</p><p>
<span class="instruct">Start at the <a href="<<<MVDparameterHref1>>>"><<<MVDparameterText3>>>*</a> dataset main page.
<br />Select the maximum temperature data variable. </span><font size="-1"><a href="<<<MVDparameterHref1>>><<<MVDparameterHref10>>>">CHECK</a></font>
<br /><span class="instruct">Note that the temperature unit is <<<MVDparameterText20>>>.</span>
<br /><span class="t">This is good because our mask threshold is also in <<<MVDparameterText20>>>. If the units had not agreed, then we would have had to convert the mask threshold to the units of the data variable.</span>
</p><p>
<span class="instruct">While in expert mode, enter the following line below the text already there.</span>
<pre class="ingridcode"><<<MVDparameterText23>>> maskgt</pre><span class="instruct">Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref1>>><<<MVDparameterHref10>>><<<MVDparameterText23>>>/maskgt/#expert">CHECK</a></font>
</p><p>
<span class="t">An analogous operation, masking out the maximum temperature values less than <<<MVDparameterText23>>>°<<<MVDparameterText1>>>, can be done by replacing <span class="ingridcode">maskgt</span> with <span class="ingridcode">masklt</span>.</span>
</p><p>
<span class="t">To see the results of this operation:</span>
<br /><span class="instruct">Select a single <<<MVDparameterText24>>> and a short time period (1994) to make the size of the data file more manageable. </span>
<font size="-1"><a href="/expert<<<MVDparameterHref1>>><<<MVDparameterHref10>>><<<MVDparameterText23>>>/maskgt/<<<MVDparameterHref11>>>T/%281994%29VALUES/#expert">CHECK</a></font>
</p><p>
<a href="/expert<<<MVDparameterHref1>>><<<MVDparameterHref10>>><<<MVDparameterText23>>>/maskgt/<<<MVDparameterHref11>>>T/%281994%29VALUES/#expert">START</a>
</p><p>
<span class="instruct">Select Tables tab > Agree button> columnar table link. </span>
<font size="-1"> <a href="/expert<<<MVDparameterHref1>>><<<MVDparameterHref10>>><<<MVDparameterText23>>>/maskgt/<<<MVDparameterHref11>>>T/%281994%29VALUES/T+exch+table-+text+text+skipanyNaN+-table+.html">CHECK</a></font>
<span class="instruct">Compare with the</span> <font size="-1"><a href="/expert<<<MVDparameterHref1>>><<<MVDparameterHref10>>><<<MVDparameterHref11>>>T/%281994%29VALUES/T+exch+table-+text+text+skipanyNaN+-table+.html">ORIGINAL DATA</a></font>
<span class="instruct">. Note that the data value from <<<MVDparameterText25>>> is missing. (Tables exclude NaN values.)</span>
</p>
</div>
<div id="Flag" class="ui-tabs-panel">
<h2>Flagging Data</h2>
<p>
<span class="t">Flags create a binary version of any variable based on a particular threshold. Those data that meet the threshold are given a value of 1 and those that do not receive a value of 0.</span>
</p><p>
<span class="ex"><u>Example: Flag <<<MVDparameterText26>>> values greater than <<<MVDparameterText27>>> <<<MVDparameterText28>>>.</u></span>
</p><p>
<span class="instruct">Start at the <a href="<<<MVDparameterHref1>>>"><<<MVDparameterText3>>>*</a> dataset main page.
<br />Select the <<<MVDparameterText26>>> data variable. </span><font size="-1"><a href="<<<MVDparameterHref1>>><<<MVDparameterHref12>>>">CHECK</a></font>
<br /><span class="instruct">Note that the <<<MVDparameterText26>>> unit is <<<MVDparameterText29>>>.</span>
<br /><span class="t">Our flag threshhold is in <<<MVDparameterText28>>>, so we must convert that <<<MVDparameterText26>>> to give Ingrid the threshhold in the units of the data variable.</span>
</p><p>
<span class="instruct">While in expert mode, enter the following line below the text already there.</span>
<pre class="ingridcode"><<<MVDparameterText30>>> flaggt</pre><span class="instruct">Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref1>>><<<MVDparameterHref12>>><<<MVDparameterText30>>>/flaggt/#expert">CHECK</a></font>
</p><p>
<span class="t">To see the results of this operation:</span>
<br /><span class="instruct">Select a single <<<MVDparameterText31>>> and a short time period (1996) to make the size of the data file more manageable. </span>
<font size="-1"><a href="/expert<<<MVDparameterHref1>>><<<MVDparameterHref12>>><<<MVDparameterText30>>>/flaggt/T/%281996%29VALUES/<<<MVDparameterHref13>>>#expert">CHECK</a></font>
</p><p>
<a href="/expert<<<MVDparameterHref1>>><<<MVDparameterHref12>>><<<MVDparameterText30>>>/flaggt/T/%281996%29VALUES/<<<MVDparameterHref13>>>#expert">START</a>
</p><p>
<span class="instruct">Select Tables tab > Agree button> columnar table link. </span>
<font size="-1"><a href="/expert<<<MVDparameterHref1>>><<<MVDparameterHref12>>><<<MVDparameterText30>>>/flaggt/T/%281996%29VALUES/<<<MVDparameterHref13>>>T+exch+table-+text+text+skipanyNaN+-table+.html">CHECK</a></font>
<span class="instruct">Compare with the <font size="-1"><a href="<<<MVDparameterHref1>>><<<MVDparameterHref12>>>T/%281996%29VALUES/<<<MVDparameterHref13>>>T+exch+table-+text+text+skipanyNaN+-table+.html">ORIGINAL DATA</a></font> </span>
<span class="instruct">.</span>
</p><p>
<span class="t">An analogous operation, flagging <<<MVDparameterText26>>> less than <<<MVDparameterText27>>> <<<MVDparameterText28>>>, can be done by replacing <i>flaggt</i> with <span class="ingridcode">flaglt</span>.</span>
</p>
</div>
</div>
</div>
<div id="Averages" class="ui-tabs-panel">
<h2>Creating Averages</h2>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
<li><a href="#Spatial">Spatial Averages</a></li>
<li><a href="#Chunk">Seasonal/Chunk Averages</a></li>
<li><a href="#Running">Running Averages</a></li>
</ul>
<div id="Spatial" class="ui-tabs-panel">
<h2>Spatial averages</h2>
<p>
<span class="t">When creating a spatial average of station of data, one typically wants to take into account the location of each station (e.g., weighted average). That operation is beyond the scope of this tutorial. However, creating a spatial average of gridded data is much more straightforward and an example is given here.</span>
</p><p>
<span class="ex"><u>Example: Find the spatial average of monthly <<<MVDparameterText5>>> data in a region in <<<MVDparameterText32>>> for Jan-Dec 1998.</u></span>
</p><p>
<span class="instruct">Start at the <a href="<<<MVDparameterHref4>>>"><<<MVDparameterText6>>>*</a>
dataset main page.
<br />Select the monthly <<<MVDparameterText5>>> data variable.</span><font size="-1"><a href="<<<MVDparameterHref4>>><<<MVDparameterHref7>>>">CHECK</a></font>
<br /><span class="instruct">Select the 1998 time period and the lat/lon defined region. </span>
<font size="-1"><a href="<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/<<<MVDparameterHref14>>>T/%28Jan%201998%29%28Dec%201998%29RANGE/">CHECK</a> <a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/<<<MVDparameterHref14>>>T/%28Jan%201998%29%28Dec%201998%29RANGE/">EXPERT</a></font>
</p><p>
<a href="<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/<<<MVDparameterHref14>>>T/%28Jan%201998%29%28Dec%201998%29RANGE/">START</a>
</p><p>
<span class="instruct">Enter expert mode and enter the following line below the text already there.</span>
<pre class="ingridcode">[X Y] average</pre><span class="instruct">Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/<<<MVDparameterHref14>>>T/%28Jan%201998%29%28Dec%201998%29RANGE/%5BX/Y%5Daverage/">CHECK</a></font>
</p><p>
<span class="t">To see the results of this operation:</span>
<br /><span class="instruct">Select one of the options in the views tab.</span>
</p><p>
<span class="t">This procedure can be easily applied to other types of spatial averaging. For example, if you wanted to create a zonal average, then you would use the following line of Ingrid
instead.</span>
<pre class="ingridcode">[X] average</pre><span class="t">This creates a zonal average as a function of T (time) and Y (latitude). Click
<a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/<<<MVDparameterHref14>>>T/%28Jan%201998%29%28Dec%201998%29RANGE/%5BX%5Daverage/figviewer.html#expert?map.url=Y+T+fig-+colors+-fig&amp;my.help=more+options">here</a> to see an example of this operation.</span>
</p>
</div>
<div id="Chunk" class="ui-tabs-panel">
<h2>Seasonal/chunk averages</h2>
<p>
<span class="ex"><u>Example: Create seasonal averages (DJF, MAM, JJA, SON) of monthly <<<MVDparameterText5>>> data from 1990-1999.</u></span>
</p><p>
<span class="instruct">Start at the <a href="<<<MVDparameterHref4>>>"><<<MVDparameterText6>>>*</a> dataset main page.
<br />Select the monthly <<<MVDparameterText5>>> data variable. </span><font size="-1"><a href="<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/">CHECK</a></font>
<br /><span class="instruct">Select the Dec 1989-Nov 1999 time period. </span>
<font size="-1"> <a href="<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/T/%28Dec%201989%29%28Nov%201999%29RANGE/">CHECK</a> <a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/T/%28Dec%201989%29%28Nov%201999%29RANGE/#expert">EXPERT</a></font>
</p><p>
<a href="<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/T/%28Dec%201989%29%28Nov%201999%29RANGE/">START</a>
</p><p>
<span class="instruct">While in expert mode, enter the following line below the text already there.
<pre class="ingridcode">T 3 boxAverage</pre>Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/T/%28Dec%201989%29%28Nov%201999%29RANGE/T/3/boxAverage/#expert">CHECK</a></font>
</p><p>
<span class="t">To see an animation of the seasonal averages you just created:</span>
<br /><span class="instruct">Select one of the options of the views tab.
<br />Enter the "Jan 1990 - Oct 1999" in the time text box at the top of the data viewer.
<br />Click "Redraw". </span><font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/T/%28Dec%201989%29%28Nov%201999%29RANGE/T/3/boxAverage/figviewer.html#expert?my.help=more+options&amp;map.T.plotvalue=Jan+1990+to+Oct+1999&amp;map.Y.units=degree_north&amp;map.url=X+Y+fig-+colors+coasts+-fig&amp;map.domainparam=+%2Fplotaxislength+432+psdef+%2Fplotborder+72+psdef+%2FXOVY+null+psdef&amp;map.zoom=Zoom&amp;map.X.units=degree_east&amp;map.X.modulus=360&amp;map.newurl.grid0=X&amp;map.newurl.grid1=Y&amp;map.newurl.land=draw+coasts&amp;map.newurl.plot=colors&amp;map.plotaxislength=432&amp;map.plotborder=72&amp;map.fnt=Helvetica&amp;map.fntsze=12&amp;map.XOVY=auto&amp;map.color_smoothing=1&amp;map.iftime=25&amp;map.mftime=25&amp;map.fftime=200">CHECK</a></font>
</p><p>
<span class="t">Note that if you had wanted JFM, AMJ, JAS, OND seasonal averages, then the selected time period would have been Jan 1990 to Dec 1999. Another important point here is that the step over which the average is created is always in the units of the data variable in question. For example, had the data been at a daily time scale, the above Ingrid command would have created a 3-day average instead of a 3-month average. Therefore, it is an excellent idea to get in the habit of making sure the units of the data variable and the step agree with each other. The technique used in this example is particularly useful when 12 is evenly divisible by the step over which you want to average. The next example addresses the cases when this is not true.</span>
</p><p>
<span class="ex"><u>Example: Create a May-Sept averages of <<<MVDparameterText8>>> anomalies data for the time period 1985-1994.</u></span>
<br /><span class="t">This example creates an average over 5 months. Twelve is not evenly divisible by this step (e.g., 5 months) so we much use a different technique than the one above.</span>
</p><p>
<span class="instruct">Start at the <a href="<<<MVDparameterHref4>>>" target="gridded"><<<MVDparameterText6>>>*</a> dataset main page.
<br />Select the <<<MVDparameterText8>>> data variable (and comptue anomalies if necessary). </span><font size="-1"><a href="<<<MVDparameterHref4>>><<<MVDparameterHref6>>>">CHECK</a></font>
<br /><span class="instruct">Select the Jan 1985- Dec 1994 time period. </span>
<font size="-1"><a href="<<<MVDparameterHref4>>><<<MVDparameterHref6>>>T/%28Jan%201985%29%28Dec%201994%29RANGE/">CHECK</a> <a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref6>>>T/%28Jan%201985%29%28Dec%201994%29RANGE/#expert">EXPERT</a></font>
</p><p>
<a href="<<<MVDparameterHref4>>><<<MVDparameterHref6>>>T/%28Jan%201985%29%28Dec%201994%29RANGE/">START</a>
</p><p>
<span class="instruct">While in expert mode, enter the following line below the text already there.
<pre class="ingridcode">T 12 splitstreamgrid</pre>Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref6>>>T/%28Jan%201985%29%28Dec%201994%29RANGE/T/12/splitstreamgrid/#expert">CHECK</a></font>
</p><p>
<span class="t">This Ingrid command splits the time grid with a period of 12. That is, in this example, it creates a dataset of Jan data, a dataset of Feb data, etc. This is an important step, but we are not quite finished.</span>
</p><p>
<span class="instruct">Select May-Sept grids and average over them with the following Ingrid commands.
<pre class="ingridcode">T (May) (Jun) (Jul) (Aug) (Sep) VALUES
<br />[T] average</pre>Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref6>>>T/%28Jan%201985%29%28Dec%201994%29RANGE/T/12/splitstreamgrid/T/%28May%29%28Jun%29%28Jul%29%28Aug%29%28Sep%29VALUES%5BT%5Daverage/#expert">CHECK</a></font>
</p><p>
<span class="t">There is also a convenient option if you want to create averages/climatologies of single months.</span>
</p><p>
<span class="ex"><u>Example: Create a monthly climatology of <<<MVDparameterText5>>> data for the time period 1982-2001.</u></span>
</p><p>
<span class="instruct">Start at the <a href="<<<MVDparameterHref4>>>"><<<MVDparameterText6>>>*</a> dataset main page.
<br />Select the monthly <<<MVDparameterText5>>> data variable. </span><font size="-1"><a href="<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/">CHECK</a></font>
<br /><span class="instruct">Select the Jan 1982-Dec 2001 time period. </span>
<font size="-1"> <a href="/<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/T/%28Jan%201982%29%28Dec%202001%29RANGE/">CHECK</a> <a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/T/%28Jan%201982%29%28Dec%202001%29RANGE/#expert">EXPERT</a></font>
</p><p>
<a href="<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/T/%28Jan%201982%29%28Dec%202001%29RANGE/">START</a>
</p><p>
<span class="instruct">Select the Filters tab.
<br />Select the monthly climatology link.</span>
<br /><font size="-1"><a href="<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/T/%28Jan%201982%29%28Dec%202001%29RANGE/yearly-climatology/">CHECK</a>
<a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/T/%28Jan%201982%29%28Dec%202001%29RANGE/yearly-climatology/#expert">EXPERT</a></font>
</p><p>
<span class="t">Note that this command can only be applied to monthly data. </span>
</p>
</div>
<div id="Running" class="ui-tabs-panel">
<h2>Running averages</h2>
<p>
<span class="t">This operation offers a fast and easy way to smooth data temporally. Let us look at an example.</span>
</p><p>
<span class="ex"><u>Example: Create a 15-day running average of <<<MVDparameterText41>>> data.</u></span>
</p><p>
<span class="instruct">Start at the <a href="<<<MVDparameterHref1>>>"><<<MVDparameterText3>>>*</a>
dataset main page.
<br />Select the <<<MVDparameterText41>>> data variable. </span><font size="-1"><a href="<<<MVDparameterHref1>>><<<MVDparameterHref3b>>>">CHECK</a></font>
</p><p>
<span class="t">At this point, it is a good habit to check the temporal unit to make sure it agrees with how you want to define your average step. In this example, we want to create a 15-day running mean. Therefore, the unit over which we want to average is a day.</span>
</p><p>
<span class="instruct">Make sure that the temporal unit of the <<<MVDparameterText41>>> data is the same as the unit over which you want to average.
<br />While in expert mode, enter the following line below the text already there.
<pre class="ingridcode">T 15 runningAverage</pre>Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref1>>><<<MVDparameterHref3b>>>T/15/runningAverage/#expert">CHECK</a></font>
</p><p>
<span class="t">Note that this operation will truncate the data to fit the step. In this example, we have a step of 15 days and are using the full time grid of <<<MVDparameterText33>>>. Therefore, after the running mean is created, the data will include the dates <<<MVDparameterText34>>>.</span>
</p><p>
<span class="t">To see the results of this operation:</span>
<br /><span class="instruct">Select a <<<MVDparameterText35>>>.</span>
</p><p>
<a href="<<<MVDparameterHref1>>><<<MVDparameterHref3b>>>T/15/runningAverage/<<<MVDparameterHref15>>>">START</a>
</p><p>
<span class="t">Select one of the views links in the function bar. Compare with those from the <a href="<<<MVDparameterHref1>>><<<MVDparameterHref3b>>>T/15/runningAverage/<<<MVDparameterHref15>>>figviewer.html?plottype=line">line</a> , <a href="<<<MVDparameterHref1>>><<<MVDparameterHref3b>>>T/15/runningAverage/<<<MVDparameterHref15>>>figviewer.html?map.url=dup+T+fig-+colorbars2+-fig">bar</a> , and <a href="<<<MVDparameterHref1>>><<<MVDparameterHref3b>>>T/15/runningAverage/<<<MVDparameterHref15>>>figviewer.html?map.url=T+exch+fig-+scatter+-fig">scatter</a> plots of the original data.</span>
</p>
</div>
</div>
</div>
<div id="Other" class="ui-tabs-panel">
<h2>Statistical and Other Mathematical Operations</h2>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
<li><a href="#Anomalies">Anomalies</a></li>
<li><a href="#Correlation">Correlation</a></li>
<li><a href="#Trig">Trigonometric Functions</a></li>
</ul>
<div id="Anomalies" class="ui-tabs-panel">
<h2>Anomalies</h2>
<p>
<span class="t">Earth science data is commonly viewed in term of anomalies (i.e., difference between observations and climatology) rather than as raw values. Anomalies can be produced with Ingrid by first calculating a climatology and then calculating the difference between it and the observed data. However, Ingrid also has a single command that does all of these calculations. Let us look at an example.</span>
</p><p>
<span class="ex"><u>Example: Recreate the <<<MVDparameterText8>>> anomalies data for the time period 1982-2001.</u></span>
</p><p>
<span class="instruct">Start at the <a href="<<<MVDparameterHref4>>>"><<<MVDparameterText6>>>*</a> dataset main page.
<br />Select the monthly <<<MVDparameterText5>>> data variable. </span><font size="-1"><a href="<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/">CHECK</a></font>
<br /><span class="instruct">Select the Jan 1982-Dec 2001 time period. </span>
<font size="-1"><a href="<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/T/%28Jan%201982%29%28Dec%202001%29RANGE/">CHECK</a> <a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/T/%28Jan%201982%29%28Dec%202001%29RANGE/#expert">EXPERT</a></font>
</p><p>
<a href="<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/T/%28Jan%201982%29%28Dec%202001%29RANGE/">START</a>
</p><p>
<span class="instruct">While in expert mode, enter the following line below the text already there.
<pre class="ingridcode">yearly-anomalies</pre>Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref7>>>/T/%28Jan%201982%29%28Dec%202001%29RANGE/yearly-anomalies/#expert">CHECK</a></font>
</p><p>
<span class="t">You have just created the <<<MVDparameterText8>>> anomalies for the time period 1982-2001 based a 1982-2001 climatology. While convenient, this operation is bit limited in that it can be applied to monthly data. And like the <i>yearly-climatology</i> command, you can find this options via the
"Filters" tab.</span>
</p>
</div>
<div id="Correlation" class="ui-tabs-panel">
<h2>Correlation</h2>
<p>
<span class="t">This is an excellent example that combines many of the techniques covered to this point.</span>
</p><p>
<span class="ex"><u>Example: Correlate precipitation amount observations in <<<MVDparameterText36>>> with <<<MVDparameterText8c>>> in the region defined by 130°-90°W, 5°-15°S for the time period 1997-1998.</u></span>
<br /><span class="tb">Note: in order to correlate two sets of data, they must have the exact same temporal unit.</span>
</p><p>
<span class="t">Let us use <<<MVDparameterText37>>>, that has monthly data.</span>
</p><p>
<span class="instruct">Select the <<<MVDparameterText37>>> dataset by either searching for it or through the SOURCES option. </span><font size="-1"><a href="<<<MVDparameterHref16>>>">CHECK</a></font>
<br /><span class="instruct">Select the precipitation variable. </span>
<font size="-1"> <a href="<<<MVDparameterHref16>>><<<MVDparameterHref18>>>">CHECK</a></font>
<br /><span class="instruct">Select the <<<MVDparameterText38>>>.</span>
<font size="-1"><a href="<<<MVDparameterHref16>>><<<MVDparameterHref18>>><<<MVDparameterHref17>>>">CHECK</a> <a href="/expert<<<MVDparameterHref16>>><<<MVDparameterHref18>>><<<MVDparameterHref17>>>#expert">EXPERT</a></font>
<br /><span class="instruct">Select the Jan 1997-Dec 1998 time period.</span>
<font size="-1"><a href="<<<MVDparameterHref16>>><<<MVDparameterHref18>>><<<MVDparameterHref17>>>T/%28Jan%201997%29%28Dec%201998%29RANGE/">CHECK</a> <a href="/expert<<<MVDparameterHref16>>><<<MVDparameterHref18>>><<<MVDparameterHref17>>>T/%28Jan%201997%29%28Dec%201998%29RANGE/#expert">EXPERT</a></font>
</p><p>
<span class="t">At this point, when the first dataset selections have been made, it is typically easiest to make the second dataset selections in expert mode.</span>
</p><p>
<span class="instruct">While in expert mode, enter the following lines below the text already there. All of these commands should look familiar to you from previous examples.
<pre class="ingridcode">
<<<MVDparameterText39>>>
T (Jan 1997) (Dec 1998) RANGE
X (130W) (90W) RANGE
Y (15S) (5S) RANGE
</pre>
Click "OK". </span>
<font size="-1"><a href="/expert<<<MVDparameterHref16>>><<<MVDparameterHref18>>><<<MVDparameterHref17>>>T/%28Jan%201997%29%28Dec%201998%29RANGE<<<MVDparameterHref4c>>><<<MVDparameterHref6c>>>T/%28Jan%201997%29%28Dec%201998%29RANGE/X/%28130W%29%2890W%29RANGE/Y/%2815S%29%285S%29RANGE/#expert">CHECK</a></font>
</p><p>
<a href="/expert<<<MVDparameterHref16>>><<<MVDparameterHref18>>><<<MVDparameterHref17>>>T/%28Jan%201997%29%28Dec%201998%29RANGE<<<MVDparameterHref4c>>><<<MVDparameterHref6c>>>T/%28Jan%201997%29%28Dec%201998%29RANGE/X/%28130W%29%2890W%29RANGE/Y/%2815S%29%285S%29RANGE/#expert" target="GHCN">START</a>
</p><p>
<span class="t">You now have two data fields with identical time grids. Let us correlate these fields.</span>
<br />
</p><p>
<span class="instruct">While in expert mode, enter the following lines below the text already there.
<pre class="ingridcode">[T] correlate</pre>Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref16>>><<<MVDparameterHref18>>><<<MVDparameterHref17>>>T/%28Jan%201997%29%28Dec%201998%29RANGE<<<MVDparameterHref4c>>><<<MVDparameterHref6c>>>T/%28Jan%201997%29%28Dec%201998%29RANGE/X/%28130W%29%2890W%29RANGE/Y/%2815S%29%285S%29RANGE/%5BT%5Dcorrelate/#expert">CHECK</a></font>
</p><p>
<span class="t">To view the correlation data you just produced:</span>
<br /><span class="instruct">Select one of the options in the views tab.</span>
<br /><span class="t">OR</span>
<br /><span class="instruct">Select Tables tab > Agree button > columnar table link. </span>
<font size="-1"><a href="/expert<<<MVDparameterHref16>>><<<MVDparameterHref18>>><<<MVDparameterHref17>>>T/%28Jan%201997%29%28Dec%201998%29RANGE<<<MVDparameterHref4c>>><<<MVDparameterHref6c>>>T/%28Jan%201997%29%28Dec%201998%29RANGE/X/%28130W%29%2890W%29RANGE/Y/%2815S%29%285S%29RANGE/%5BT%5Dcorrelate/ngridtable+table-+skipanyNaN+3+-table+.html">CHECK</a></font>
<br /><span class="t">You can correlate over the spatial grids as well by replacing [T] with [X], [Y], [X Y], etc.</span>
</p>
</div>
<div id="Trig" class="ui-tabs-panel">
<h2>Trigonometric functions</h2>
<p>
<span class="t">Basic trig functions are typically used with the spatial grids. The results of this function can then be used as part a broader technique, such as spatial weighting.</span>
</p><p>
<span class="ex"><u>Example: Find the cosine of a latitudinal grid of <<<MVDparameterText40>>> data.</u></span>
</p><p>
<span class="instruct">Start at the <a href="<<<MVDparameterHref4>>>"><<<MVDparameterText6>>>*</a> dataset main page.
<br />Select the <<<MVDparameterText40>>> data variable. </span><font size="-1"><a href="<<<MVDparameterHref4>>><<<MVDparameterHref19>>>">CHECK</a></font>
<br /><span class="instruct">While in expert mode, enter the following line after the text already there.</span>
<pre class="ingridcode">Y cosd</pre>
<span class="instruct">Click "OK". </span><font size="-1"><a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref19>>>Y/cosd/#expert">CHECK</a></font>
<br /><span class="t">To find the sine of data, replace cosd with <span class="ingridcode">sind</span>.</span>
</p><p>
<span class="t">To view the data you just produced:</span>
<br /><span class="instruct">Select one of the options in the views tab.</span>
<br /><span class="t">OR</span>
<br /><span class="instruct">Select Tables tab > columnar table link.</span><font size="-1"> <a href="/expert<<<MVDparameterHref4>>><<<MVDparameterHref19>>>Y/cosd/Y+exch+table-+text+text+skipanyNaN+-table+.html">CHECK</a></font>
</p></div>
</div>
</div>
</div>
<div class="optionsBar">
<fieldset class="navitem" id="share"><legend>Share</legend></fieldset>
<fieldset class="navitem" id="contactus"></fieldset>
</div>
</body>
</html>
